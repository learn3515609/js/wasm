function alertDoubleValue(value){
  alert(value * 2);
}

function logValue(value) {
  console.log(value);
}

let importObject = { 
  env: {
      update: alertDoubleValue 
  }
}

WebAssembly.instantiateStreaming(
  fetch("counter.wasm"), importObject
).then(result => {
  logValue(result.instance.exports.count());
  logValue(result.instance.exports.count());
  logValue(result.instance.exports.count());
  logValue(result.instance.exports.count());
}
).catch(console.error);